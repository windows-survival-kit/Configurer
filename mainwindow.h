/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_build_clicked();

    void on_actionAbout_Qt_triggered();

    void on_actionWindows_Survival_Kit_website_triggered();

    void on_actionLicence_triggered();

    void on_backgroundInfo_clicked();

public slots:
    void initDownloadBar(QString format);
    void initInstallBar();
    void setDownloadProgress(int value, int max);
    void installDone();
    void addToLog(QString text);

private:
    Ui::MainWindow *ui;
    void enableAll(bool enable);
};

#endif // MAINWINDOW_H
