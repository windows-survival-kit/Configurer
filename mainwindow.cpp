/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "appmanager.h"
#include "fileutils.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/filesystem/operations.hpp>
#include <QFuture>
#include <QtConcurrentRun>
#include <QDesktopServices>
#include <QFileDialog>
#include <QDir>
#include <QUrl>
#include <QMessageBox>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    ui->backgroundInfo->setIcon(this->style()->standardIcon(QStyle::SP_MessageBoxInformation));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::enableAll(bool enable) {
    ui->background->setEnabled(enable);
    ui->backgroundEnable->setEnabled(enable);
    ui->startMenuFolder->setEnabled(enable);
    ui->startMenuFolderLabel->setEnabled(enable);
    ui->software->setEnabled(enable);

    ui->soft_firefox->setEnabled(enable); // row 1
    ui->soft_7zip->setEnabled(enable);
    ui->soft_qbittorrent->setEnabled(enable); // row 2
    ui->soft_notepadplusplus->setEnabled(enable);
    ui->soft_libreoffice->setEnabled(enable); // row 3
    ui->soft_gimp->setEnabled(enable);

    ui->build->setEnabled(enable);
}

void MainWindow::on_build_clicked() {
    QString fileName = QFileDialog::getExistingDirectory(this, "Select a file...", QDir::homePath());
    if(fileName.length() == 0) return;
    string outPath = fileName.toUtf8().constData();
    outPath += "/WindowsSurvivalKit";
    string exePath = outPath + ".exe";

    if(boost::filesystem::exists(outPath) || boost::filesystem::exists(exePath)) {
        QMessageBox::warning(this, "File already exists!",
                             "Either WindowsSurvivalKit or WindowsSurvivalKit.exe already exist in the selected folder",
                             QMessageBox::Ok, QMessageBox::Ok);

        //        cout << "path already used!" << endl;
        return;
    }

    // disable everything
    enableAll(false);

    // set up the progress bar, to indeterminite mode
    ui->progressBar->setEnabled(true);
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(0);

    string appPath = outPath + "/apps";
    string backgroundPath = outPath + "/backgrounds";

    mkdir(outPath);
    mkdir(appPath);
    mkdir(backgroundPath);
    ofstream conf((outPath + "/conf.ini").c_str());
    if(ui->backgroundEnable->isChecked()) {
        string background = ui->background->currentText().toUtf8().constData();
        background = background + ".jpeg";
        conf << "background=<appdir>/backgrounds/" << background << endl;

        // would ideally be in download thread, though it's almost instant here
        boost::filesystem::copy("data/backgrounds/" + background, backgroundPath + "/" + background);
    }
    if(!ui->startMenuFolder->text().isEmpty()) {
        string menuFolder = ui->startMenuFolder->text().toUtf8().constData();
        conf << "startFolder=" << menuFolder << endl;
    }
    conf.close();

    // should move to the download thread, but it's fast enough.
    boost::filesystem::copy("data/WindowsSurvivalKit.exe", exePath);

    vector<ManagedApp> *apps = new vector<ManagedApp>;
    if(ui->soft_firefox->isChecked()) {
        apps->push_back(ManagedApp("firefox", this));
    }
    if(ui->soft_7zip->isChecked()) {
        apps->push_back(ManagedApp("7zip", this));
    }
    if(ui->soft_qbittorrent->isChecked()) {
        apps->push_back(ManagedApp("qbittorrent", this));
    }
    if(ui->soft_notepadplusplus->isChecked()) {
        apps->push_back(ManagedApp("notepad++", this));
    }
    if(ui->soft_libreoffice->isChecked()) {
        apps->push_back(ManagedApp("libreoffice", this));
    }
    if(ui->soft_gimp->isChecked()) {
        apps->push_back(ManagedApp("gimp", this));
    }

    QFuture<void> future = QtConcurrent::run(build, apps, this, outPath, appPath);
}

void MainWindow::on_actionAbout_Qt_triggered() {
    QApplication::aboutQt();
}

void MainWindow::initDownloadBar(QString format) {
    // make sure the progress bar is enabled
    ui->progressBar->setEnabled(true);

    // set the text on it
    ui->progressBar->setFormat(format);
}

void MainWindow::setDownloadProgress(int value, int max) {
    ui->progressBar->setMaximum(max);
    ui->progressBar->setValue(value);
}

void MainWindow::initInstallBar() {
    // set up the progress bar, to indeterminite mode
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(0);

    // set the text
    ui->progressBar->setFormat(QString("Installing..."));
}

void MainWindow::installDone() {
    // installation is done
    // set everything back

    ui->progressBar->setMaximum(1);
    ui->progressBar->setEnabled(false);
    ui->progressBar->setFormat(QString(""));

    // reenable everything
    enableAll(true);

    QMessageBox::information(this, "Done!",
                             "Windows Survival Kit has been installed! Run WindowsSurvivalKit.exe on a computer to set it up.",
                             QMessageBox::Ok, QMessageBox::Ok);
}

void MainWindow::addToLog(QString text) {
    ui->log->appendPlainText(text);
}

void MainWindow::on_actionWindows_Survival_Kit_website_triggered() {
    QDesktopServices::openUrl(QUrl("https://windows-survival-kit.gitlab.io/"));
}

void MainWindow::on_actionLicence_triggered() {
    QDesktopServices::openUrl(QUrl("https://www.gnu.org/licenses/gpl.html"));
}

void MainWindow::on_backgroundInfo_clicked() {
    std::ifstream in("data/backgrounds/Licences", std::ios::in | std::ios::binary);
    std::ostringstream contents;
    contents << in.rdbuf();
    in.close();
    QMessageBox::information(this, "Background Info",
                             QString::fromStdString(contents.str()),
                             QMessageBox::Ok, QMessageBox::Ok);
}
