/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <iostream>
#include <fstream>

#include "appmanager.h"
#include "config.h"
#include "fileutils.h"

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <QApplication>

string getcwd_string() {
    char buff[PATH_MAX];
    getcwd( buff, PATH_MAX );
    std::string cwd( buff );
    return cwd;
}

void log(MainWindow *main, string text) {
    // send it to the main window
    QMetaObject::invokeMethod(main, "addToLog", Qt::QueuedConnection,
                              Q_ARG( QString, QString::fromStdString(text) ));
}

ManagedApp::ManagedApp(string app, MainWindow* parent) : app(app), parent(parent) {
}

void ManagedApp::downloadApp() {
    string confPath = "data/apps/" + app + ".ini";
    string home = getenv("HOME");
    downloadDir = home + "/.windowssurvivalkit/downloads/";
    mkdir(home + "/.windowssurvivalkit");
    mkdir(downloadDir);

    configuration::data config;
    ifstream f(confPath.c_str());
    f >> config;
    f.close();

    string url = config["url"];
    installerFileName = config["filename"];
    string temp_filename = downloadDir + installerFileName;

    launcher = config["launcher"];

    if(!boost::filesystem::exists(temp_filename)) {
        log(parent, "Downloading " + app);

        // so that things don't break if the download is interrupted
        string downloadName = temp_filename + ".tmp";
        download(this, url, downloadName);
        boost::filesystem::rename(downloadName, temp_filename);
        cout << "Done downloading." << endl;
        log(parent, "Done downloading.");
    } else {
        log(parent, "App " + app + " has already been downloaded, skipping.");
    }
}

string ManagedApp::installApp(string appPath) {
    string filename = appPath + "/" + installerFileName;
    string temp_filename = downloadDir + installerFileName;

    string command = "wine " + filename;

    log(parent, "Installing app " + app);

    boost::filesystem::rename(temp_filename, filename);
    system(command.c_str());
    boost::filesystem::rename(filename, temp_filename);

    return "<appdir>\\apps\\" + launcher;
}

void ManagedApp::updateProgress(int done, int todo) {

    // convert everything into KB
    done /= 1024;
    todo /= 1024;

    // move progress bar
    QMetaObject::invokeMethod(parent, "setDownloadProgress", Qt::QueuedConnection,
                              Q_ARG( int, done ), Q_ARG( int, todo ) );
}

void ManagedApp::setupProgress() {
    // make the format string
    QString format = QString::fromStdString("Downloading " + app + " %p% (%v/%mKiB)");

    // send it away
    QMetaObject::invokeMethod(parent, "initDownloadBar", Qt::QueuedConnection,
                              Q_ARG( QString, format ));
}

void build(vector<ManagedApp> *apps, MainWindow *main, string outPath, string appPath) {
    // download the apps
    log(main, " --- Downloading apps ...");
    for(vector<ManagedApp>::iterator app = apps->begin(); app != apps->end(); ++app) {
        app->downloadApp();
    }

    // set up the progress bar, to indeterminite mode
    log(main, " --- Installing apps ...");
    QMetaObject::invokeMethod(main, "initInstallBar", Qt::QueuedConnection);

    // install the apps, and add them to apps.txt
    ofstream appsList((outPath + "/apps.txt").c_str());
    for(vector<ManagedApp>::iterator app = apps->begin(); app != apps->end(); ++app) {
        appsList << app->installApp(appPath) << endl;
    }
    appsList.close();
    delete apps;

    // tell the program that we're done
    log(main, " --- Done ---");
    QMetaObject::invokeMethod(main, "installDone", Qt::QueuedConnection);
}

