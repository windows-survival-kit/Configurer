#-------------------------------------------------
#
# Project created by QtCreator 2015-12-23T14:36:04
#
#-------------------------------------------------

QT       += core gui network widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Windows-Survival-Kit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    appmanager.cpp \
    config.cpp \
    fileutils.cpp

HEADERS  += mainwindow.h \
    appmanager.h \
    config.h \
    fileutils.h

FORMS    += mainwindow.ui

LIBS += -lboost_system -lboost_filesystem -lcurl

copydata.commands = $(COPY_DIR) \"$$PWD/data\" \"$$OUT_PWD\"
QMAKE_EXTRA_TARGETS += copydata
PRE_TARGETDEPS += copydata
