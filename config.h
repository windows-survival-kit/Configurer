#include <map>
#include <string>

#ifndef CONFIG_H
#define CONFIG_H

namespace configuration {
    struct data: std::map <std::string, std::string> {
        // Here is a little convenience method...
        bool iskey( const std::string& s ) const;
    };

    //---------------------------------------------------------------------------
    // The extraction operator reads configuration::data until EOF.
    // Invalid data is ignored.
    //
    std::istream& operator >> ( std::istream& ins, data& d );
    std::ostream& operator << ( std::ostream& outs, const data& d );
} // namespace configuration

#endif
