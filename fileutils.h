/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <string>
#include <boost/filesystem/path.hpp>
#include <sys/stat.h>
#include "appmanager.h"

using namespace std;

void mkdir(string path);

void download(ManagedApp* app, string url, string filename);

bool copyDir(
        boost::filesystem::path const & source,
        boost::filesystem::path const & destination
        );

#endif // FILEUTILS_H
