﻿/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef APPMANAGER_H
#define APPMANAGER_H

#include <string>
#include <sys/types.h>
#include <boost/filesystem/path.hpp>
#include <QProgressBar>
#include "mainwindow.h"

using namespace std;

class ManagedApp {
private:
    string app;
    string launcher;
    string installerFileName;
    string downloadDir;
    MainWindow* parent;
public:
    ManagedApp(string app, MainWindow* parent);
    void downloadApp();
    string installApp(string appPath);
    void updateProgress(int done, int todo);
    void setupProgress();
};

void build(vector<ManagedApp> *apps, MainWindow *main, string outPath, string appPath);

#endif // APPMANAGER_H
