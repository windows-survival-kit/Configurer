/**
This file is part of Windows Survival Kit.

Windows Survival Kit is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Windows Survival Kit is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Windows Survival Kit.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "fileutils.h"
#include <boost/filesystem/operations.hpp>
#include <curl/curl.h>

bool copyDir(
        boost::filesystem::path const & source,
        boost::filesystem::path const & destination
        ) {
    namespace fs = boost::filesystem;
    try {
        // Check whether the function call is valid
        if(
                !fs::exists(source) ||
                !fs::is_directory(source)
                ) {
            std::cerr << "Source directory " << source.string()
                      << " does not exist or is not a directory." << '\n'
                         ;
            return false;
        }
        if(fs::exists(destination)) {
            std::cerr << "Destination directory " << destination.string()
                      << " already exists." << '\n'
                         ;
            return false;
        }
        // Create the destination directory
        if(!fs::create_directory(destination)) {
            std::cerr << "Unable to create destination directory"
                      << destination.string() << '\n'
                         ;
            return false;
        }
    } catch(fs::filesystem_error const & e) {
        std::cerr << e.what() << '\n';
        return false;
    }
    // Iterate through the source directory
    for(
        fs::directory_iterator file(source);
        file != fs::directory_iterator(); ++file
        ) {
        try
        {
            fs::path current(file->path());
            if(fs::is_directory(current))
            {
                // Found directory: Recursion
                if(
                        !copyDir(
                            current,
                            destination / current.filename()
                            )
                        )
                {
                    return false;
                }
            }
            else
            {
                // Found file: Copy
                fs::copy_file(
                            current,
                            destination / current.filename()
                            );
            }
        }
        catch(fs::filesystem_error const & e)
        {
            std:: cerr << e.what() << '\n';
        }
    }
    return true;
}

void mkdir(string path) {
    mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}


static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
    size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
    return written;
}

static int progress(void *p,
                    double dltotal, double dlnow,
                    double ultotal, double ulnow) {
    // supress unused variable warning
    ultotal += ulnow;

    // update progress
    struct ManagedApp *app = (struct ManagedApp *)p;
    app->updateProgress(dlnow, dltotal);

    return 0;
}

void download(ManagedApp* app, string url, string filename) {
    app->setupProgress();

    CURL *curl_handle;
    FILE *pagefile;

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curl_handle = curl_easy_init();

    /* set URL to get here */
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());

    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 0L); // 1L for debug

    // enable progress meter, as this is used to update the progress bar in the GUI
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 0L);

    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

    /* sourceforge downloads are redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

    // setup the progress meter
    curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, progress);
    // pass the struct pointer into the progress function
    curl_easy_setopt(curl_handle, CURLOPT_PROGRESSDATA, app);

    /* open the file */
    pagefile = fopen(filename.c_str(), "wb");
    if (pagefile) {

        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);

        /* get it! */
        curl_easy_perform(curl_handle);

        /* close the header file */
        fclose(pagefile);
    }

    /* cleanup curl stuff */
    curl_easy_cleanup(curl_handle);

    return;
}

